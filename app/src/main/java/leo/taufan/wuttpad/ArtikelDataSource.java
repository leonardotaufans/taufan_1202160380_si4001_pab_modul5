package leo.taufan.wuttpad;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

class ArtikelDataSource {

    private SQLiteDatabase database;
    private ArtikelDB artikelDB;
    private String[] allColumns = {ArtikelDB.COLUMN_ID, ArtikelDB.JUDUL_ARTIKEL,
            ArtikelDB.AUTHOR_ARTIKEL, ArtikelDB.DESC_ARTIKEL, ArtikelDB.CREATED_DATE};

    //the initialize stuff
    public ArtikelDataSource(Context context) {
        artikelDB = new ArtikelDB(context);
    }

    //starting database
    public void open() throws SQLException {
        database = artikelDB.getWritableDatabase();
    }

    //closing database
    public void close() throws SQLException {
        artikelDB.close();
    }

    //for making new article
    public Artikel createArtikel(String judul, String author, String desc, String date) {
        ContentValues values = new ContentValues();
        values.put(ArtikelDB.JUDUL_ARTIKEL, judul);
        values.put(ArtikelDB.AUTHOR_ARTIKEL, author);
        values.put(ArtikelDB.DESC_ARTIKEL, desc);
        values.put(ArtikelDB.CREATED_DATE, date);

        long insertId = database.insert(ArtikelDB.TABLE_NAME, null, values);
        //cursor is...pointer. Directing where on database it currently reads
        Cursor cursor = database.query(ArtikelDB.TABLE_NAME, allColumns, ArtikelDB.COLUMN_ID
                + " = " +insertId, null, null, null, null);
        cursor.moveToFirst();
        Artikel newArtikel = cursorToArtikel(cursor);
        cursor.close();
        return newArtikel;
    }

    private Artikel cursorToArtikel(Cursor cursor) {

        //cursor points it, and links it to Artikel class
        Artikel artikel = new Artikel();
        //tester
        Log.v("TESTER", "LONG"+cursor.getLong(0));
        Log.v("TESTER", "setOthers"+cursor.getString(1)+", "+cursor.getString(2));
        artikel.setId(cursor.getLong(0));
        artikel.setTitle(cursor.getString(1));
        artikel.setAuthor(cursor.getString(2));
        artikel.setDesc(cursor.getString(3));
        artikel.setCreated(cursor.getString(4));
        return artikel;
    }

    public ArrayList<Artikel> readArtikel() {
        //will return entire list of article.
        ArrayList<Artikel> alArtikel = new ArrayList<Artikel>();
        Cursor cursor = database.query(artikelDB.TABLE_NAME, allColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            Artikel artikel = cursorToArtikel(cursor);
            alArtikel.add(artikel);
            cursor.moveToNext();
        }
        cursor.close();
        return alArtikel;
    }

    public Artikel findArtikelById(long id) {
        //unlike what it believes, findArtikelById is USED! I think ,-,
        Artikel artikel = new Artikel();
        Cursor cursor = database.query(ArtikelDB.TABLE_NAME, allColumns, "_id="+id, null, null, null, null);
        cursor.moveToFirst();
        artikel = cursorToArtikel(cursor);
        cursor.close();
        return artikel;
    }

    public void updateArtikelById(Artikel a) {
        //Update artikel by filter _id
        String mFilter = "_id="+a.getId();
        ContentValues args = new ContentValues();
        args.put(ArtikelDB.JUDUL_ARTIKEL, a.getTitle());
        args.put(ArtikelDB.AUTHOR_ARTIKEL, a.getAuthor());
        args.put(ArtikelDB.DESC_ARTIKEL, a.getDesc());
        args.put(ArtikelDB.CREATED_DATE, a.getCreated());
        database.update(ArtikelDB.TABLE_NAME, args, mFilter, null);
    }

    public void deleteArtikelById(long id) {
        String mFilter = "_id="+id;
        database.delete(ArtikelDB.TABLE_NAME, mFilter, null);
    }
    public void deleteAllArtikel() {
        //Unused, scrapped idea
        database.delete(ArtikelDB.TABLE_NAME, "1", null);
        database.close();
    }
}
