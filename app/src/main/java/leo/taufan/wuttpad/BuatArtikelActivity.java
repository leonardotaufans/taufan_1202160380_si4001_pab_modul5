package leo.taufan.wuttpad;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BuatArtikelActivity extends AppCompatActivity {

        private CoordinatorLayout coord_buat;
        private EditText edJudul, edAuthor, edTanggal, edDesc;
        private ArtikelDataSource artikelDB;
        boolean theme;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //Check theme, font
            SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
            boolean theme = preferences.getBoolean("dark_theme", false);
            boolean font = preferences.getBoolean("font_large", false);
            if (theme && font) {
                setTheme(R.style.AppTheme_Dark_FontLarge);
            } else if (theme) {
                setTheme(R.style.AppTheme_Dark_FontNormal);
            } else if (font) {
                setTheme(R.style.AppTheme_FontLarge);
            }
            setContentView(R.layout.activity_buat_artikel);
            initUI();
        }

        private void initUI() {
            edJudul = findViewById(R.id.buat_edittext_judul);
            edAuthor = findViewById(R.id.buat_edittext_author);
            edTanggal = findViewById(R.id.buat_edittext_tanggal);
            //control so edTanggal will launch DatePicker instead
            edTanggal.setFocusable(false);
            edTanggal.setClickable(true);
            //Set default date to today
            Date date = Calendar.getInstance().getTime();
            SimpleDateFormat defaultDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            edTanggal.setText(defaultDateFormat.format(date));
            //onClickListener for DatePickerDialog
            edTanggal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setTanggal();
                }
            });
            edDesc = findViewById(R.id.buat_edittext_desc);
            coord_buat = findViewById(R.id.buat_coordinator);
            //Preparing the artikel database
            artikelDB = new ArtikelDataSource(this);
            artikelDB.open();
        }

        private void setTanggal() {
            Calendar calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(BuatArtikelActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, month, dayOfMonth);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                    edTanggal.setText(dateFormat.format(newDate.getTime()));
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }

        public void buatArtikel(View view) {
            String judul, author, tanggal, desc;
            Artikel artikel;
            //Text checker
            if (TextUtils.isEmpty(edJudul.getText())) {
                Snackbar.make(coord_buat, "Judul wajib diisi!", Snackbar.LENGTH_LONG)
                        .show();
                edJudul.setError("Judul wajib diisi!");
                return;
            }
            if (TextUtils.isEmpty(edAuthor.getText())) {
                Snackbar.make(coord_buat, "Nama penulis wajib diisi!", Snackbar.LENGTH_LONG)
                        .show();
                edAuthor.setError("Nama penulis wajib diisi!");
                return;
            }
            if (TextUtils.isEmpty(edTanggal.getText())) {
                Snackbar.make(coord_buat, "Tanggal tidak benar!", Snackbar.LENGTH_LONG)
                        .show();
                edTanggal.setError("Tanggal tidak benar!");
                return;
            }
            if (TextUtils.isEmpty(edDesc.getText())) {
                Snackbar.make(coord_buat, "Deskripsi wajib diisi!", Snackbar.LENGTH_LONG)
                        .show();
                edJudul.setError("Deskripsi wajib diisi!");
                return;
            }
            judul = edJudul.getText().toString();
            author = edAuthor.getText().toString();
            tanggal = edTanggal.getText().toString();
            desc = edDesc.getText().toString();
            //Set them to ArtikelDB
            artikel = artikelDB.createArtikel(judul, author, desc, tanggal);
            Toast.makeText(this, "Data terinput! \n"
                    + artikel.getTitle(), Toast.LENGTH_LONG)
                    .show();
            finish();


        }
    }

