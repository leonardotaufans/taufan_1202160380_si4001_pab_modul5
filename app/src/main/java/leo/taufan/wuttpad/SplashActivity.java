package leo.taufan.wuttpad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    TextView textView;
    ProgressBar progressBar;
    int i = 0;
    Timer timer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setProgress(0);
        final long period = 100;
        timer = new Timer();
        textView = findViewById(R.id.Wuttpad);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //Splash screen using progressBar, set on timer
                if (i <100) {
                    //timer using different thread than UI, so use runOnUiThread
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (i <= 30) {
                                textView.setText("Collecting chakra... please wait.");
                            } else if (i <= 60) {
                                textView.setText("Did you know? \nThis loading is a lie.");
                            } else if (i <=90) {
                                textView.setText("Did you know? \nThere is nothing being loaded right now.");
                            } else if (i >91) {
                                textView.setText("Welcome! \nJust need a little more hug...");
                            }
                        }
                    });
                    progressBar.setProgress(i);
                    //change this value below for FASTEEER POWAAAAAAA
                    i++;

                } else {
                    //stop timer, start mainactivity
                    timer.cancel();
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }
        }, 0, period);
    }
}
