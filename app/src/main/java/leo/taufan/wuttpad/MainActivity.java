package leo.taufan.wuttpad;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    CoordinatorLayout coord_main;
    boolean theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Launching sharedPreferences, checking...
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        theme = preferences.getBoolean("dark_theme", false);
        boolean font = preferences.getBoolean("font_large", false);
        //Setting both theme and font, controlled by theme
        if (theme && font) {
            setTheme(R.style.AppTheme_Dark_FontLarge);
        } else if (theme) {
            setTheme(R.style.AppTheme_Dark_FontNormal);
        } else if (font) {
            setTheme(R.style.AppTheme_FontLarge);
        }
        setContentView(R.layout.activity_main);
        coord_main = findViewById(R.id.main_coordinator);
    }

    public void viewArtikel(View view) {
        //Button to view Artikel
        Intent intent = new Intent(MainActivity.this, ViewArtikel.class);
        startActivity(intent);
    }

    public void buatArtikel(View view) {
        Intent intent = new Intent(MainActivity.this, BuatArtikelActivity.class);
        startActivity(intent);
    }

    public void settings(View view) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        //Alert on back pressed
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("Tutup")
             .setMessage("Apakah Anda yakin untuk menutup aplikasi ini?")
             .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                 @Override
                 public void onClick(DialogInterface dialog, int which) {
                    finish();
                    }
             }).setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }
}
