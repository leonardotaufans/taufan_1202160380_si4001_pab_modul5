package leo.taufan.wuttpad;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditArtikel extends AppCompatActivity {

    private ArtikelDataSource db;
    private List<Artikel> values;
    private Artikel artikel = new Artikel();
    private EditText edJudul, edAuthor, edDate, edDesc;
    Long id;
    String judul, author, date, desc;
    private CoordinatorLayout coord_edit;
    boolean theme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Check theme
        SharedPreferences preferences = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean theme = preferences.getBoolean("dark_theme", false);
        boolean font = preferences.getBoolean("font_large", false);
        if (theme && font) {
            setTheme(R.style.AppTheme_Dark_FontLarge);
        } else if (theme) {
            setTheme(R.style.AppTheme_Dark_FontNormal);
        } else if (font) {
            setTheme(R.style.AppTheme_FontLarge);
        }
        setContentView(R.layout.activity_edit_artikel);
        //Prepare database
        db = new ArtikelDataSource(this);
        db.open();
        initUI();


    }

    private void initUI() {
        Bundle i = getIntent().getExtras();
        //Get bundle for editing. Becomes default values
        if (i != null) {
            id = i.getLong("id");
            judul = i.getString("judul");
            author = i.getString("author");
            date = i.getString("date");
            desc = i.getString("desc");
        }
        edJudul = findViewById(R.id.edit_edittext_judul);
        edAuthor = findViewById(R.id.edit_edittext_author);
        edDate = findViewById(R.id.edit_edittext_tanggal);
        //OnClickListener for EditText date
        edDate.setFocusable(false);
        edDate.setClickable(true);
        edDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_tanggal();
            }
        });
        edDesc = findViewById(R.id.edit_edittext_desc);
        coord_edit = findViewById(R.id.edit_coordinator);
        edJudul.setText(judul);
        edDesc.setText(desc);
        edDate.setText(date);
        edAuthor.setText(author);
    }

    private void edit_tanggal() {
        //The usual DatePickerDialog stuff
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(EditArtikel.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                edDate.setText(dateFormat.format(newDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    public void updateArtikel(View view) {
        if (TextUtils.isEmpty(edJudul.getText())) {
            Snackbar.make(coord_edit, "Judul wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edJudul.setError("Judul wajib diisi!");
            return;
        }
        if (TextUtils.isEmpty(edAuthor.getText())) {
            Snackbar.make(coord_edit, "Nama penulis wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edAuthor.setError("Nama penulis wajib diisi!");
            return;
        }
        if (TextUtils.isEmpty(edDate.getText())) {
            Snackbar.make(coord_edit, "Tanggal tidak benar!", Snackbar.LENGTH_LONG)
                    .show();
            edDate.setError("Tanggal tidak benar!");
            return;
        }
        if (TextUtils.isEmpty(edDesc.getText())) {
            Snackbar.make(coord_edit, "Deskripsi wajib diisi!", Snackbar.LENGTH_LONG)
                    .show();
            edJudul.setError("Deskripsi wajib diisi!");
            return;
        }
        Log.d("EDIT", edJudul.getText().toString());
        artikel.setId(id);
        artikel.setTitle(edJudul.getText().toString());
        artikel.setAuthor(edAuthor.getText().toString());
        artikel.setCreated(edDate.getText().toString());
        artikel.setDesc(edDesc.getText().toString());
        db.updateArtikelById(artikel);
        Intent intent = new Intent(EditArtikel.this, ViewArtikel.class);
        startActivity(intent);
        db.close();
        Log.d("EDITARTICLE", artikel.getId().toString());
        finish();

    }
}

