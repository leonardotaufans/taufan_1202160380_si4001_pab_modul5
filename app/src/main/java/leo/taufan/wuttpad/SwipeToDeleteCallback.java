package leo.taufan.wuttpad;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
//Used for swipe to delete feature.
    ArtikelAdapter mAdapter;
    Context context;

    public SwipeToDeleteCallback(ArtikelAdapter context) {
        super(ItemTouchHelper.LEFT, ItemTouchHelper.RIGHT);
        this.mAdapter = context;
    }

    public SwipeToDeleteCallback(int dragDirs, int swipeDirs) {
        //not really used
        super(dragDirs, swipeDirs);
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        //Detect direction of swipe. Only detecting left or right swipe
        return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return false;
    }

    @Override
    public float getSwipeThreshold(@NonNull RecyclerView.ViewHolder viewHolder) {
        //value before swipe is accepted
        return 0.7f;
    }

    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        //going to ArtikelAdapter.deleteData() method
        mAdapter.deleteData(viewHolder.getAdapterPosition());

    }
}
